# Cursus42 - Libft

You can find the subject there : [Subject](https://gitlab.com/jeanbenoit.rossignol.eleve/cursus42/-/blob/norminette/Subject.pdf)

You can also find the norms there : [Norms](https://gitlab.com/jeanbenoit.rossignol.eleve/cursus42/-/blob/norminette/Norminette.pdf)

# Getting started

You can use the library for yourself althought most of the function in there already exist in other libraries.

### Prerequisites

- Install gcc\
Using brew :
`brew install gcc`\
Linux os :
`packagemanager gcc`

- Install make\
Using brew :
`brew install make`\
Linux os :
`packagemanager make`

- Install binutils for ar\
Using brew :
`brew install binutils`\
Linux os :
`packagemanager binutils`

### Commands

- `make` : compile both first and second part to make the library file.
- `make bonus` : compile both first and second aswell as the bonus part to make the library file.
- `make re` : remove all the object files and will do the make command after.
- `make clean` : remove all the object files.
- `make fclean` : remove the library file and do the clean command after.

### Using the library

Using the `make` or `make bonus` command will create the libft.a file.
You will need to include the header file at the beginning of the file like so : `#include "libft.h"`\
To use it, you must compile the program using gcc and use the -L flag and -l.

- L flag : looks in directory for library files.
- l flag : adds include directory of header files.

If you used my Makefile, the command to hook the library to your program would be : `gcc yourmain.c -L. -lft`

# Tester

During the process of recoding all the functions, I would not have been able to do it without the help\
of tester people made for this project! I used three tester to make sure my code was working as intended\
The link to the repo of those tester will be in the [Sources](#4--Sources)

# Sources

### Flags
1. [L flag](https://www.rapidtables.com/code/linux/gcc/gcc-l.html)
2. [l flag](https://www.rapidtables.com/code/linux/gcc/gcc-i.html)

### Tester
3. [Tripouille](https://github.com/Tripouille/libftTester)
4. [Warmachine](https://github.com/ska42/libft-war-machine)
5. [Libft-unit-test](https://github.com/alelievr/libft-unit-test)

